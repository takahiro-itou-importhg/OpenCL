﻿//  -*-  mode: c++; coding: utf-8  -*-  //
/*************************************************************************
**                                                                      **
**                      ---   OpenCL Wrapper   ---                      **
**                                                                      **
**      Copyright (c), Takahiro Itou, 2013-2014.                        **
**      All Rights Reserved.                                            **
**                                                                      **
*************************************************************************/

/**
**      Sample Program.
**
**      @file       Sample/Sample.cpp
**/

#include    "../Common/OclWrap.h"
#include    "../Common/SimpleTimer.h"

#include    <fstream>
#include    <sstream>
#include    <iostream>

void
runOnHost(
        const  float  *   input1,
        const  float  *   input2,
        float  *          output,
        const  size_t     NUM)
{
    for ( size_t index = 0; index < NUM; ++ index ) {
        output[index] += (input1[index] * input2[index]);
    }
    return;
}


int  main(int argc, char * argv[])
{
    cl_int          err = CL_SUCCESS;

    const  size_t   NUM = (argc >= 2 ? atol(argv[1]) : 1000000);
    std::vector<float>  vecInput1(NUM);
    std::vector<float>  vecInput2(NUM);
    std::vector<float>  vecOutput1(NUM);
    std::vector<float>  vecOutput2(NUM);

    float  *  const  input1 = &(vecInput1[0]);
    float  *  const  input2 = &(vecInput2[0]);
    float  *  const  output1= &(vecOutput1[0]);
    float  *  const  output2= &(vecOutput2[0]);

    CLWRAP::OclWrap wrapper;

    //  データの準備。  //
    for ( size_t i = 0; i < NUM; ++ i ) {
        input1[i]   = (float)i;
        input2[i]   = (float)i;
        output1[i]  = 0.0f;
        output2[i]  = 0.0f;
    }

    //  Running By GPU  //
    CLWRAP::SimpleTimer  timer;
    CLWRAP::SimpleTimer  timeGPU;

    timer.startTimer();
    try {
        wrapper.setupInstance(std::cerr);
        wrapper.readSourceFile("Sample.cl");
        wrapper.buildPrograms(std::cerr);
        cl::Kernel  kernel(wrapper.getProgram(), "addVector", &err);

        cl::Buffer  memInput1   = wrapper.createBuffer(
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                sizeof(cl_float) * NUM, input1, &err);

        cl::Buffer  memInput2   = wrapper.createBuffer(
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                sizeof(cl_float) * NUM, input2, &err);

        cl::Buffer  memOutput   = wrapper.createBuffer(
                CL_MEM_WRITE_ONLY,
                sizeof(cl_float) * NUM, NULL, &err);

        kernel.setArg(0, memInput1);
        kernel.setArg(1, memInput2);
        kernel.setArg(2, memOutput);

        cl::CommandQueue    queue   = wrapper.createCommandQueue(0, &err);

        timeGPU.startTimer();
        queue.enqueueNDRangeKernel(kernel, cl::NullRange,
            cl::NDRange(NUM), cl::NullRange, NULL, NULL);

        queue.enqueueNDRangeKernel(kernel, cl::NullRange,
            cl::NDRange(NUM), cl::NullRange, NULL, NULL);
        timeGPU.stopTimer();

        queue.enqueueReadBuffer(memOutput, CL_TRUE, 0,
            sizeof(float) * NUM, output1, NULL, NULL);

    } catch ( std::exception & err ) {
        std::cerr   << "ERROR : " << err.what() << std::endl;
        exit( EXIT_FAILURE );
    } catch ( ... ) {
        std::cerr   << "Unknown Exception" << std::endl;
        exit( EXIT_FAILURE );
    }
    timer.stopTimer();

    std::cerr   << "Run On GPU --- "
                << "CLOCK : "  << timer.getClockTime()
                << ", REAL : " << timer.getRealTime()
                << std::endl;
    std::cerr   << "In Kernel  --- "
                << "CLOCK : "  << timeGPU.getClockTime()
                << ", REAL : " << timeGPU.getRealTime()
                << std::endl;
    for ( int i = 0; i  < 20; ++ i ) {
        std::cout   << "input1[" << i
                    << "], input2[" << i
                    << "], output[" << i << "] : "
                    << input1[i] << ", "
                    << input2[i] << ", "
                    << output1[i] << std::endl;
    }

    //  Running By CPU  //
    timer.startTimer();
    runOnHost(input1, input2, output2, NUM);
    runOnHost(input1, input2, output2, NUM);
    timer.stopTimer();
    std::cerr   << "Run On CPU --- "
                << "CLOCK : "  << timer.getClockTime()
                << ", REAL : " << timer.getRealTime()
                << std::endl;

    for ( int i = 0; i  < 20; ++ i ) {
        std::cout   << "input1[" << i
                    << "], input2[" << i
                    << "], output[" << i << "] : "
                    << input1[i] << ", "
                    << input2[i] << ", "
                    << output2[i] << std::endl;
    }

    return ( 0 );
}
