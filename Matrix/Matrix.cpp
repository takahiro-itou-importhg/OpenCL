﻿//  -*-  mode: c++; coding: utf-8  -*-  //
/*************************************************************************
**                                                                      **
**                      ---   OpenCL Wrapper   ---                      **
**                                                                      **
**      Copyright (c), Takahiro Itou, 2013-2014.                        **
**      All Rights Reserved.                                            **
**                                                                      **
*************************************************************************/

/**
**      行列乗算のサンプル。
**
**      @file       Matrix/Matrix.cpp
**/

#include    "../Common/OclWrap.h"
#include    "../Common/SimpleTimer.h"

#include    <fstream>
#include    <sstream>
#include    <iomanip>
#include    <iostream>

#define     KERNEL_TYPE     2

const  int  BLOCK   = 16;
const  int  WIDTH   = 2048;


void
runOnHost(
        const  float  *   input1,
        const  float  *   input2,
        float  *          output,
        const  size_t     nWidth)
{
    for ( size_t row = 0; row < nWidth; ++ row ) {
        for ( size_t col = 0; col < nWidth; ++ col ) {
            float   tmp = 0.0;
            for ( size_t k = 0; k < nWidth; ++ k ) {
                const   size_t  ik  = (row * nWidth) + k;
                const   size_t  kj  = (k * nWidth) + col;
                tmp += input1[ik] * input2[kj];
            }
            output[row * nWidth + col]  = tmp;
        }
    }
    return;
}


int  main(int argc, char * argv[])
{
    cl_int          err = CL_SUCCESS;

    const  int  NUM = (argc >= 2 ? atol(argv[1]) : WIDTH);
    std::vector<float>  vecInput1(NUM * NUM);
    std::vector<float>  vecInput2(NUM * NUM);
    std::vector<float>  vecOutput(NUM * NUM);

    float  *  const  input1 = &(vecInput1[0]);
    float  *  const  input2 = &(vecInput2[0]);
    float  *  const  output = &(vecOutput[0]);

    //  データの準備。  //
    for ( int i = 0; i < NUM; ++ i ) {
        for ( int j = 0; j < NUM; ++ j ) {
            input1[i * NUM + j] = (float)(i + j);
            input2[i * NUM + j] = (float)(i + j);
            output[i * NUM + j] = 0.0f;
        }
    }

    std::cerr   << "START ..." << std::endl;

    //  Running By GPU  //
    CLWRAP::SimpleTimer  timer;
    CLWRAP::SimpleTimer  timeGPU;

    timer.startTimer();
    try {
        CLWRAP::OclWrap wrapper;

        wrapper.setupInstance(std::cerr);
        wrapper.readSourceFile("Matrix.cl");
        wrapper.buildPrograms(std::cerr);
#if ( KERNEL_TYPE == 1 )
        cl::Kernel  kernel(wrapper.getProgram(), "mulMatrix1", &err);
#else
        cl::Kernel  kernel(wrapper.getProgram(), "mulMatrix2", &err);
#endif

        cl::Buffer  memInput1   = wrapper.createBuffer(
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                sizeof(cl_float) * NUM * NUM, input1, &err);

        cl::Buffer  memInput2   = wrapper.createBuffer(
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                sizeof(cl_float) * NUM * NUM, input2, &err);

        cl::Buffer  memOutput1  = wrapper.createBuffer(
                CL_MEM_READ_WRITE,
                sizeof(cl_float) * NUM * NUM, NULL, &err);

        cl::Buffer  memOutput2  = wrapper.createBuffer(
                CL_MEM_READ_WRITE,
                sizeof(cl_float) * NUM * NUM, NULL, &err);

        kernel.setArg(0, memInput1);
        kernel.setArg(1, memInput2);
        kernel.setArg(2, memOutput1);
        kernel.setArg(3, NUM);
#if ( KERNEL_TYPE == 2 )
        kernel.setArg(4, sizeof(cl_float) * BLOCK * BLOCK, NULL);
        kernel.setArg(5, sizeof(cl_float) * BLOCK * BLOCK, NULL);
        kernel.setArg(6, BLOCK);
#endif
        cl::CommandQueue    queue   = wrapper.createCommandQueue(0, &err);

        timeGPU.startTimer();
        queue.enqueueNDRangeKernel(kernel, cl::NullRange,
                cl::NDRange(NUM, NUM), cl::NDRange(BLOCK, BLOCK), NULL, NULL);

        kernel.setArg(0, memOutput1);
        kernel.setArg(1, memInput2);
        kernel.setArg(2, memOutput2);
#if ( KERNEL_TYPE == 2 )
        kernel.setArg(4, sizeof(cl_float) * BLOCK * BLOCK, NULL);
        kernel.setArg(5, sizeof(cl_float) * BLOCK * BLOCK, NULL);
#endif

        queue.enqueueNDRangeKernel(kernel, cl::NullRange,
                cl::NDRange(NUM, NUM), cl::NDRange(BLOCK, BLOCK), NULL, NULL);
        timeGPU.stopTimer();

        queue.enqueueReadBuffer(memOutput2, CL_TRUE, 0,
            sizeof(float) * NUM * NUM, output, NULL, NULL);

    } catch ( std::exception & err ) {
        std::cerr   << "ERROR : " << err.what() << std::endl;
        exit( EXIT_FAILURE );
    } catch ( ... ) {
        std::cerr   << "Unknown Exception" << std::endl;
        exit( EXIT_FAILURE );
    }
    timer.stopTimer();

    std::cerr   << "Run On GPU [ms]--- "
                << "CLOCK : "  << timer.getClockTime() * 1000
                << ", REAL : " << timer.getRealTime() * 1000
                << std::endl;
    std::cerr   << "In Kernel  --- "
                << "CLOCK : "  << timeGPU.getClockTime() * 1000
                << ", REAL : " << timeGPU.getRealTime() * 1000
                << std::endl;

    //  デバッグ用に一部分だけ表示する。    //
#if defined( OCLPROJ_MATRIX_DEBUG_DUMP_RESULT )
    for ( int i = 0; i < 8; ++ i ) {
        for ( int j = 0; j < 8; ++ j ) {
            std::cout   << output[i * NUM + j] << ", ";
        }
        std::cout   << std::endl;
    }
#endif

    //  Running By CPU  //
    timer.startTimer();
    runOnHost(input1, input2, output, NUM);
    runOnHost(output, input2, input1, NUM);
    timer.stopTimer();
    std::cerr   << "Run On CPU [ms] --- "
                << "CLOCK : "  << timer.getClockTime() * 1000
                << ", REAL : " << timer.getRealTime() * 1000
                << std::endl;

#if defined( OCLPROJ_MATRIX_DEBUG_DUMP_RESULT )
    for ( int i = 0; i < 8; ++ i ) {
        for ( int j = 0; j < 8; ++ j ) {
            std::cout   << input1[i * NUM + j] << ", ";
        }
        std::cout   << std::endl;
    }
#endif

    return ( 0 );
}
