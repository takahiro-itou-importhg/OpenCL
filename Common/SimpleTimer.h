﻿//  -*-  mode: c++; coding: utf-8  -*-  //
/*************************************************************************
**                                                                      **
**                      ---   OpenCL Wrapper   ---                      **
**                                                                      **
**      Copyright (c), Takahiro Itou, 2013-2014.                        **
**      All Rights Reserved.                                            **
**                                                                      **
*************************************************************************/

/**
**      An Interface of SimpleTimer class.
**
**      @file       Common/SimpleTimer.h
**/


#if !defined( OCLPROJ_COMMON_INCLUDED_SIMPLE_TIMER_H )
#    define   OCLPROJ_COMMON_INCLUDED_SIMPLE_TIMER_H

#pragma     once

#include    <time.h>

namespace  CLWRAP  {

//========================================================================
//
//    SimpleTimer  class.
//
/**
**    時間計測クラス。
**/

class  SimpleTimer
{
public:
    void    startTimer();
    void    stopTimer();
    double  getRealTime()   const   { return ( this->m_tmReal ); }
    double  getClockTime()  const   { return ( this->m_tmClock ); }
private:
    clock_t m_ctStart;
    int     m_rtStart;
    double  m_tmClock;
    double  m_tmReal;
};

}   //  End of namespace  CLWRAP

#endif
