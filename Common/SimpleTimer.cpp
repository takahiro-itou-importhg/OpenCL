﻿//  -*-  mode: c++; coding: utf-8  -*-  //
/*************************************************************************
**                                                                      **
**                      ---   OpenCL Wrapper   ---                      **
**                                                                      **
**      Copyright (c), Takahiro Itou, 2013-2014.                        **
**      All Rights Reserved.                                            **
**                                                                      **
*************************************************************************/

/**
**      An Implementation of SimpleTimer class.
**
**      @file       Common/SimpleTimer.cpp
**/

#include    "SimpleTimer.h"

#include    <time.h>

#include    <windows.h>
#include    <mmsystem.h>
#pragma     comment(lib, "winmm.lib")

namespace  CLWRAP  {

void
SimpleTimer::startTimer()
{
    this->m_rtStart = ::timeGetTime();
    this->m_ctStart = clock();
}

void
SimpleTimer::stopTimer()
{
    clock_t ctLast  = ::clock();
    DWORD   rtLast  = ::timeGetTime();

    this->m_tmReal  = (rtLast - this->m_rtStart) * 1e-3;
    this->m_tmClock = static_cast<double>(ctLast - this->m_ctStart)
                        / CLOCKS_PER_SEC;
    return;
}

}   //  End of namespace  CLWRAP
